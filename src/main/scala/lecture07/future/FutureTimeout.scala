package lecture07.future

import java.util.{Timer, TimerTask}

import scala.concurrent.{Await, ExecutionContext, Future, Promise, TimeoutException}
import scala.concurrent.duration._

object FutureTimeout {

  def addTimeoutAwait[A](fu: Future[A], timeout: FiniteDuration, exception: => Exception)(implicit ec: ExecutionContext): Future[A] =
    Future {
      Await.result(fu, timeout)
    }.recoverWith {
      case _: TimeoutException => Future.failed(exception)
    }

  val timer = new Timer(true)
  def addTimeout[A](fu: Future[A], timeout: FiniteDuration, exception: => Exception)(implicit ec: ExecutionContext): Future[A] = {
    val p = Promise[A]

    val task = new TimerTask {
      override def run(): Unit = {
        p.tryFailure(exception)
      }
    }
    timer.schedule(task, timeout.toMillis)
    // Неплохо было бы подчистить потом за собой в timer...

    Future.firstCompletedOf(List(fu, p.future))
  }

}

object Example extends App {
  import scala.concurrent.ExecutionContext.Implicits.global

  def myFuture() = Future[String] {
    println(System.currentTimeMillis())
    Thread.sleep((3.seconds).toMillis)
    println(System.currentTimeMillis())
    "Sad"
  }

  val futureWithTimeout5 = FutureTimeout.addTimeout(myFuture(), 5.seconds, new Exception)
  println(Await.result(futureWithTimeout5, Duration.Inf))

  val futureWithTimeout1 = FutureTimeout.addTimeout(myFuture(), 1.seconds, new Exception)
  println(Await.result(futureWithTimeout1, Duration.Inf))
}
