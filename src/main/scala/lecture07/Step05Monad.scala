package lecture07

class Step05Monad {

  trait Monad[F[_]] {
    def pure[A](a: A): F[A]
    def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]

    def map[A, B](fa: F[A])(f: A => B): F[B] =
      flatMap(fa)(a => pure(f(a)))
  }

  object Monad {
    object Syntax {
      implicit class MonadOps[F[_], A](fa: F[A]) {
        def map[B](f: A => B)(implicit F: Monad[F]): F[B] = F.map(fa)(f)
        def flatMap[B](f: A => F[B])(implicit F: Monad[F]): F[B] = F.flatMap(fa)(f)
      }
    }
  }

  case class Writer[A](pair: (List[String], A))

  object Writer {
    implicit val writerC: Monad[Writer] = new Monad[Writer] {

      override def pure[A](a: A): Writer[A] =
        Writer(List.empty, a)

      override def flatMap[A, B](fa: Writer[A])(f: A => Writer[B]): Writer[B] = {
        val (log1, a) = fa.pair
        val (log2, b) = f(a).pair
        Writer((log1 ++ log2, b))
      }
    }
  }
}
