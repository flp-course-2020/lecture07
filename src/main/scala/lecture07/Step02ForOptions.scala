package lecture07

object Step02Example1 {
  import Step01Example1._

  val film = for {
    eastwood <- directors.find(_.name == "Clint Eastwood") // Option[Director]
    unforgiven <- eastwood.films.find(_.name == "Unforgiven") // Option[Film]
  } yield unforgiven

  val none = for {
    eastwood <- directors.find(_.name == "WrongName")
    unforgiven <- eastwood.films.find(_.name == "Unforgiven")
  } yield unforgiven
}
